package amazon.bo.reuse;

import amazon.bo.AmazonProduct;

public class TopBookWithDetails extends TopBooks {
    @Override
    public void createList(String keywords) {
        search.setKeywords(keywords);
        result = service.search(search);

        AmazonProduct[] p = result.getProducts();
        for (int i=0; i< p.length; i++) {
            AmazonProduct sim = service.getProductByAsin(p[i].getAsin());
            p[i] = sim;
        }
    }
}
