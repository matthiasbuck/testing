package kunden;

import java.util.ArrayList;
import java.util.List;

public class Kunde {
	private String name; // Name des Kunden
	private KontoInformationen standard; // jeder Kunde hat ein _StandardKonto_
	private List<KontoInformationen> konten; // jeder Kunde kann aber auch
												// mehrere Konten besitzen

	public Kunde(String kundenName) {
		this.name = kundenName;
		konten = new ArrayList<KontoInformationen>();
	}

	public void add(int ktoNummer, int blz) {
		if (standard == null)
			// das 1. Konto eines Kunden ist sein Standardkonto
			standard = new KontoInformationen(ktoNummer, blz);
		konten.add(standard);
	}

	/**
	 * 
	 * @return Alle Konten eines Kunden
	 */
	public List<KontoInformationen> konten() {
		return (konten);
	}

	public KontoInformationen getStandardKonto() {
		return (standard);
	}

	public String getName() {
		return (name);
	}

}
