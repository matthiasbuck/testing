package lieferant;
import brauerei.Getraenk;


public interface Lieferant {
	/**
	 * @param name "WAS" moechte ich kaufen (z.B. "Bier")
	 * @param alter Alter des Kaeufers im Sinne des Jugendschutzes
	 * @return Objekt der Elternklasse Getraenk, kann ein
	 *         Bier oder eine Cola sein oder null bei Fehler.
	 */
	Getraenk verkaufeGetraenk(String name, int alter);
}
