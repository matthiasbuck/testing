package amazon.bo.reuse;

import amazon.bo.AmazonResult;
import amazon.bo.AmazonSearch;
import amazon.bo.service.SearchService;
import amazon.export.HtmlOutput;

public class TopBooks {
	 protected AmazonSearch search;
	 protected AmazonResult result;
	 protected SearchService service;

	public TopBooks() {
		service = new SearchService();
		search = new AmazonSearch();
	}

	public void createList(String keywords) {
		search.setKeywords(keywords);
		result = service.search(search);
	}

	public void printList2Console() {
		for (int i = 0; result!=null &&
			i < result.getProducts().length; i++) {
			System.out.println(result.getProducts()[i].toString());
		}
	}

	public void printList2Html() {
		HtmlOutput out = new HtmlOutput(search.getKeywords());
		out.generateHtmlLayout(result.getProducts());
		out.createHtmlFile(search.getKeywords() + ".html");

	}
}
