package bank;


public class KontoFactory {
	/**
	 * Factory zum Anlegen von Konten
	 * @param kontoArt Art des Konto - SparKonto oder GiroKonto
	 * @param betrag Höhe der initialen Einzahlung
	 * @return erzeugtes Konto
	 * @throws Exception
	 */
	public static Konto ermittleKontoArt(String kontoArt, float betrag) throws Exception {
		if (kontoArt.equalsIgnoreCase("GiroKonto")){
			return new GiroKonto(new Transaktion(betrag));
		}
		else if(kontoArt.equalsIgnoreCase("Sparkonto")){
			return new SparKonto(new Transaktion(betrag));
		} else {
			throw new Exception("Kontotyp unbekannt");
		}
	}
	
}
