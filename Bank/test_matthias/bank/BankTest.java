package bank;

import static org.junit.Assert.*;

import java.util.List;

import kunden.KontoInformationen;
import kunden.Kunde;

import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import staat.TransaktionsMelder;

public class BankTest {

	Kunde kunde1;
	Kunde kunde2;
	Bank bank;
	
	@Mock
	TransaktionsMelder melder;
	
	@Before
	public void setUp() throws Exception {
		bank = new Bank("ksk", 65050110);
		melder = Mockito.mock(TransaktionsMelder.class);
	}

	@Test
	public void testSetzeTransaktionsmelder() {
		
		//Durchführung
		bank.setzeTransaktionsmelder(melder);
		
	}

	@Test
	public void testKontoEroeffnenOhneGeld() throws Exception {
		
		try {
			bank.kontoEroeffnen("GiroKonto", "Hans", 0);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Transaktion ungueltig: Konto nicht angelegt");
		}
	}
	
	@Test
	public void testKontoEroeffnenOhneKundennamen() {
		try {
			bank.kontoEroeffnen("GiroKonto", null, 0);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Transaktion ungueltig: Konto nicht angelegt");
		}
	}
	
	@Test
	public void testKontoEroeffnenErfolgsfall() throws Exception {
		kunde1 = bank.kontoEroeffnen("Girokonto", "Hans", 100);
		List<KontoInformationen> kontoinfos = kunde1.konten();
		
		assertEquals(kontoinfos.size(), 1);
		assertNotNull(kontoinfos.get(0).abfragenKontonummer());
		
	}

	@Test
	public void testAbfragenKontostand() throws Exception {
		kunde1 = bank.kontoEroeffnen("Girokonto", "Hans", 100);
		List<KontoInformationen> kontoinfos = kunde1.konten();
		
		assertEquals(100, bank.abfragenKontostand(kontoinfos.get(0).abfragenKontonummer()),0.1);
	
		
	}

	@Test
	public void testAbfragenKontoBesitzer() throws Exception {
		kunde1 = bank.kontoEroeffnen("Girokonto", "Hans", 100);
		List<KontoInformationen> kontoinfos = kunde1.konten();
		
		assertEquals(bank.abfragenKontoBesitzer(kontoinfos.get(0).abfragenKontonummer()), "Hans");
	}
	
	@Test
	public void testAbfragenKontoBesitzerMitFalscherNummer() {	
		try {
			kunde1 = bank.kontoEroeffnen("Girokonto", "Hans", 100);
			List<KontoInformationen> kontoinfos = kunde1.konten();
			bank.abfragenKontoBesitzer(kontoinfos.get(0).abfragenKontonummer()-1);
			fail("Should not arrive here");
			
		} catch (Exception e) {
			
			assertEquals(e.getMessage(),"Kontonummer unbekannt");
		}
	}

	
	@Test
	public void testAbhebenMitUnbekannterKontonummer() {
		try {	
			bank.abheben(1, 100);
			fail("Should not arrive here, no Bank accounts exist");
			
		} catch (Exception e) {
			
			assertEquals(e.getMessage(),"Kontonummer unbekannt");
		}
	}

	@Test
	public void testEinzahlen() throws Exception {
		
		kunde1 = bank.kontoEroeffnen("Girokonto", "Hans", 100);
		List<KontoInformationen> kontoinfos = kunde1.konten();
		
		assertEquals(100, bank.abfragenKontostand(kontoinfos.get(0).abfragenKontonummer()),0.1);
		
		bank.einzahlen(kontoinfos.get(0).abfragenKontonummer(), 1);
		
		assertEquals(101, bank.abfragenKontostand(kontoinfos.get(0).abfragenKontonummer()),0.1);
	}

	@Test
	public void testTransfer() throws Exception {
		
		kunde1 = bank.kontoEroeffnen("Girokonto", "Hans", 100);
		kunde2 = bank.kontoEroeffnen("Girokonto", "Günni", 100);
		List<KontoInformationen> kontoinfos1 = kunde1.konten();
		List<KontoInformationen> kontoinfos2 = kunde2.konten();
		
		bank.transfer(kontoinfos1.get(0).abfragenKontonummer(), kontoinfos2.get(0).abfragenKontonummer(), 50);
		
		assertEquals(bank.abfragenKontostand(kontoinfos1.get(0).abfragenKontonummer()), 50, 0.1);
		assertEquals(bank.abfragenKontostand(kontoinfos2.get(0).abfragenKontonummer()), 150, 0.1);
		
		try {
			bank.transfer(kontoinfos1.get(0).abfragenKontonummer(), kontoinfos2.get(0).abfragenKontonummer(), 1000);
			fail("sollte nicht klappen");
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Dispo ueberschritten");
		}
	}

}
