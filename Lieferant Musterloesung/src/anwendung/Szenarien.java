package anwendung;

import lieferant.Lieferant;
import student.Student;
import anwendung.monitoring.Logging;

public class Szenarien {

	public void einfachesKaeuferSzenario(Lieferant supermarkt,
			Student einStudent, Student klaus, Lieferant weitererLieferant ) {

		/* Student peter ist trocken */
		Logging.print(einStudent);

		/* Student soll beim Supermarkt ein Bier kaufen */
		einStudent.kaufeGetraenk(supermarkt, "Bier");
		Logging.print(einStudent);

		/* Trinkt 100 ml von seinem Getraenk */
		einStudent.trinke(100);
		Logging.print(einStudent);

		/* Peter trinkt alles aus */
		einStudent.trinkeAlles();
		Logging.print(einStudent);

		/* Damit kann Supermarkt nicht dienen */
		einStudent.kaufeGetraenk(supermarkt, "Kamillentee");
		Logging.print(einStudent);

		/* Auch trinken geht nicht :( */
		if (!einStudent.trinke(400)) {
			System.out.println("(peter.trinke(400) == false)");
		}

		/* Cola gibts noch */
		einStudent.kaufeGetraenk(supermarkt, "Cola");
		einStudent.trinke(300);
		Logging.print(einStudent);

		/* peter ist nett und kann sein Getraenk auch verkaufen */
		klaus.kaufeGetraenk(weitererLieferant, "Cola");
		Logging.print(einStudent);
		Logging.print(klaus);
	}

	public void studentKauftGetraenk(Student student,
			Lieferant lieferant, String getraenk) {
		Logging.print(student);

		student.kaufeGetraenk(lieferant, getraenk);
		Logging.print(student);

		student.trinke(100);
		Logging.print(student);

		student.trinkeAlles();
		Logging.print(student);


	}
}
