package student;

import static org.junit.Assert.*;
import laden.Supermarkt;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import brauerei.Brauerei;
import brauerei.Cola;
import brauerei.Getraenk;

public class StudentTest {
	
	@Mock
	private Getraenk getraenk;
	
	@Mock
	private Supermarkt supermarkt;
	
	@InjectMocks
	Student student;
	
	
	@Before
	public void setUp() throws Exception {
	student = new Student("Hans", 18);
	MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testStudent() {
		assertTrue(student.getClass().equals(Student.class));
		assertEquals(student.getName(), "Hans");
		assertNotNull(student.getName());
		//assertTrue((student.getraenk == null));
	}

	@Test
	public void testGetName() {
		assertEquals(student.getName(), "Hans");
		assertNotEquals(student.getName(), "Peter");
	}

	@Test
	public void testTrinke() {
		assertFalse(student.trinke(1));
		
		student.getraenk = new Cola(1);
		Mockito.when(getraenk.reduziereMenge(1)).thenReturn(true);
		assertTrue(student.trinke(1));
		
		student.getraenk = new Cola(1);
		assertTrue(student.trinke(1));
		assertNull(student.getraenk);
		
		student.getraenk = new Cola(2);
		assertTrue(student.trinke(1));
		assertNotNull(student.getraenk);
	}

	@Test
	public void testTrinkeAlles() {
		
		assertFalse(student.trinkeAlles());
		
		Mockito.when(getraenk.reduziereMenge(1)).thenReturn(true);
		student.getraenk = new Cola(1);
		assertTrue(student.trinkeAlles());
	}

	@Test
	public void testKaufeGetraenk() {
		assertFalse(student.kaufeGetraenk(supermarkt, "Cola"));
		
		student.trinkeAlles();
		Mockito.when(supermarkt.verkaufeGetraenk("Cola", 18)).thenReturn(new Cola(5));
		assertTrue(student.kaufeGetraenk(supermarkt, "Cola"));
		assertEquals(student.getraenk.getName(), "Cola");
		assertEquals(student.getraenk.getMenge(), 5);
		
		student.getraenk = new Cola(1);
		supermarkt = new Supermarkt("Bierladen", new Brauerei("Bierbrauer"));
		assertFalse(student.kaufeGetraenk(supermarkt, "Cola"));
		
	}

	@Test
	public void testStatus() {
	
		student = new Student("Hans", 18);
		assertEquals(student.status(), student.getName() + ": Habe kein Getraenk!");
		
		student.getraenk = new Cola(1);
		assertEquals(student.status(), student.getName() + ": " + student.getraenk.getBeschreibung());
	}

}
