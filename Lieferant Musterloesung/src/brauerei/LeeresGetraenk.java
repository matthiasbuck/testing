package brauerei;

/**
 * Ein LeeresGetraenk ist ein spezielles Getraenk, dass man nicht trinken kann ;-)
 */
public class LeeresGetraenk extends Getraenk {

	LeeresGetraenk() {
		super(0);
	}

	@Override
	public String getName() {
		return("Leeres Getraenk");
	}

	@Override
	public String getBeschreibung() {
		return("Leeres Getraenk mit keinem Inhalt");
	}

}
