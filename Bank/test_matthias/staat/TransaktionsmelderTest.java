package staat;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

public class TransaktionsmelderTest {

	TransaktionsMelder tm;
	LinkedList<String> testmessage;
	
	@Before
	public void setUp() throws Exception {
		tm = new TransaktionsMelder();
		testmessage = new LinkedList<String>();
		testmessage.add("testmessage");
	}

	@Test
	public void testMelden() {
		tm.melden("testmessage");
		assertEquals(TransaktionsMelder.testing, testmessage);
	}

	@Test
	public void testToString() {
		
	}

}
