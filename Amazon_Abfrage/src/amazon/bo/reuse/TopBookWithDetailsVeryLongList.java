package amazon.bo.reuse;

import amazon.bo.AmazonProduct;
import amazon.bo.AmazonResult;

public class TopBookWithDetailsVeryLongList extends TopBooks {
	protected int max = 0;

	public TopBookWithDetailsVeryLongList(int max) {
		this.max = max;
		assert (max > 0);
	}


	@Override
	public void createList(String keywords) {
		search.setKeywords(keywords);

		int pages = max / 10;
		int rest = max % 10;

		AmazonProduct[] myProducts = new AmazonProduct[max];

		for (int j = 0; j < pages; j++) {
			AmazonResult res = service.request(search, (j + 1));
			AmazonProduct[] p = res.getProducts();
			for (int i = 0; i < p.length; i++) {
				AmazonProduct sim = service.getProductByAsin(p[i].getAsin());
				myProducts[i + (10 * j)] = sim;
			}

		}
		AmazonResult res = service.request(search, (pages+1));
		AmazonProduct[] p = res.getProducts();
		for (int i = 0; i < rest; i++) {
			AmazonProduct sim = service.getProductByAsin(p[i].getAsin());
			myProducts[i + (10 * pages)] = sim;
		}

		this.result = new AmazonResult();
		this.result.setBooks(myProducts);
	}
}
