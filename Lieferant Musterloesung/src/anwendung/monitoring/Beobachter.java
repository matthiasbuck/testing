package anwendung.monitoring;

public interface Beobachter {
	String status();
}
