package amazon.bo.usecases.reuse;


import amazon.bo.reuse.TopBooks;

public class UC200 {
	public static void main(String[] args) {
		TopBooks top = new TopBooks();
		// Liste fuer Lenny Kravitz erstellen
		top.createList("apple");
		// Liste auf der Console ausgeben
		top.printList2Console();
		// Liste in einer Web-Seite ausgeben
		top.printList2Html();
	}
}
