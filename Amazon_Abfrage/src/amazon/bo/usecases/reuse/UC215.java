package amazon.bo.usecases.reuse;

import amazon.bo.reuse.TopBookWithVeryLongImageList;

/**
 * Sehr lange Top bilderlartige Listen von Buechern
 *
 * @author schimkar
 *
 */
public class UC215 {

	public static void main(String[] args) {
		TopBookWithVeryLongImageList topDetails = new TopBookWithVeryLongImageList(
				80);
		topDetails.createList("apple");
		topDetails.printList2Console();
		topDetails.printList2Html();
	}
}
