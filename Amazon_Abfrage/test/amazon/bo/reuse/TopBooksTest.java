package amazon.bo.reuse;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import amazon.bo.AmazonResult;
import amazon.bo.AmazonSearch;
import amazon.bo.service.SearchService;

public class TopBooksTest {
	
	
	@Mock
	protected AmazonSearch search;
	@Mock
	protected AmazonResult result;
	@Mock
	protected SearchService service;
	
	@InjectMocks
	protected TopBooks topbooks;
	
	@Before
	public void setUp() throws Exception {
		topbooks = new TopBooks();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testCreateList() {
		AmazonResult amznresult = new AmazonResult(); 
		Mockito.when(topbooks.service.search(search)).thenReturn(amznresult);
		
		assertTrue(topbooks.service.search(search).equals(amznresult));
	}

	@Test
	public void testPrintList2Console() {
		fail("Not yet implemented");
	}

	@Test
	public void testPrintList2Html() {
		fail("Not yet implemented");
	}

}
