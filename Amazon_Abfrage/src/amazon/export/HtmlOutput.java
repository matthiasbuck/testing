package amazon.export;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

import amazon.bo.AmazonProduct;

/**
 * Class for generating a simple HTML file listing entries
 *
 * @author rds
 *
 */
public class HtmlOutput {

	protected StringWriter writer = null;

	/**
	 *
	 * @param nameOfList
	 *            Search Keywords
	 */
	public HtmlOutput(String nameOfList) {
		writer = new StringWriter();
		if (nameOfList != null) {
			writer.write("<html>");
			writer.write("<title> OOP - " + nameOfList + "</title>");
			writer.write("<h2>OOP @amazon</h1>");
			writer.write("<h2>" + nameOfList + "</h2>");
		}
	}

	/**
	 * Constructor which does not generate any html-header information
	 */
	public HtmlOutput() {
		this(null);
	}

	/**
	 *
	 * @param item
	 *            to append in the list as html
	 */
	public void appendItem(String item) {
		if (writer != null)
			writer.write("<li>" + item + "</li>\n");
	}

	/**
	 * must be called to finalize the HTML file
	 */
	public void footer() {

		try {
			if (writer != null) {
				writer.write("</html>");
				writer.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * generate a layout based on HTML which can be displayed by any
	 * browser
	 * @param prod array of products
	 * @return
	 */
	public boolean generateHtmlLayout(AmazonProduct[] prod) {
		if (prod == null || prod.length == 0)
			return (false);
		writer.write("<ol>");
		for (int i = 0; i < prod.length; i++) {
			StringBuffer buf = new StringBuffer(100);
			AmazonProduct p = prod[i];
			if (p != null) {
				if (p.getImage() != null)
					buf.append("<img src=\"" + p.getImage() + "\"> ");
				buf.append(p.getSalesRank() + " - ");
				if (p.getUrl() != null)
					buf.append("<a href=\"" + p.getUrl() + "\"> "
							+ p.getTitle() + "</a>,");
				else
					buf.append(p.getTitle() + ", ");
				buf.append(" " + p.getFormattedPrice());

				// �hnliche B�cher
				HtmlOutput h = new HtmlOutput();
				h.generateHtmlLayout(p.getSimilarBooks());
				buf.append(h.asHtml());
			}
			// Preisinformationen
			appendItem(buf.toString());
		}
		writer.write("</ol>");
		return (true);
	}

	/**
	 *
	 * @return a String containing the complete HTML code
	 */
	public String asHtml() {
		return (writer.getBuffer().toString());
	}

	/**
	 *
	 * @param fileName
	 *            File Name
	 * @param asHtml
	 *            HTML Code
	 * @return
	 */
	public boolean createHtmlFile(String fileName, String asHtml) {
		try {
			String html = null;
			if (asHtml == null) {
				html = this.asHtml();
			} else
				html = asHtml;
			// In eine Datei schreiben
			FileWriter f = new FileWriter(new File(fileName));
			f.write(html);
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
			return (false);
		}
		return (true);

	}

	/**
	 *
	 * @param fileName
	 */
	public void createHtmlFile(String fileName) {
		createHtmlFile(fileName, null);
	}
}
