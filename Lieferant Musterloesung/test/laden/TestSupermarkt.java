package laden;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import brauerei.Bier;
import brauerei.Brauerei;
import brauerei.Cola;
import brauerei.Getraenk;

public class TestSupermarkt {

	//@Mock
	Brauerei brauerei;
	
	@InjectMocks
	Supermarkt supermarkt;
	
	@Before
	public void setUp() throws Exception {
		brauerei = Mockito.mock(Brauerei.class);
		//Mock(brauerei.getClass());
		Mockito.when(brauerei.verkaufeGetraenk("Bier", 18)).thenReturn(new Bier(5));
		supermarkt = new Supermarkt("Bierhaus", brauerei);
		MockitoAnnotations.initMocks(this);
		
	}

	@Test
	public void testVerkaufeGetraenk() {
		Mockito.when(supermarkt.verkaufeGetraenk("Bier", 16)).thenReturn(new Bier(5));
		Getraenk[] sortiment = new Getraenk[1];
		sortiment[0] = new Bier(16);
		supermarkt.setSortiment(sortiment);
		//assertEquals(supermarkt.verkaufeGetraenk("Bier", 16).getName(), new Bier(5).getName());
		//assertNotEquals(supermarkt.verkaufeGetraenk("Bier", 16).getName(), new Cola(5).getName());
		
		//Verhaltensorientierte Test
		//Mockito.verify(brauerei.verkaufeGetraenk("Bier", 16));
		//Mockito.verify(supermarkt.verkaufeGetraenk("bier", 16));
	}
	
	@Test
	public void testfuelleSortiment(){
		//Vorbereitung
		supermarkt.setSortiment(null);
		assertNull(supermarkt.getSortiment());
		
		
		//Durchführung
		supermarkt.fuelleSortiment();
		
		//Prüfen
		assertNotNull(supermarkt.getSortiment());
		
		
		
	}

}
