package student;

import brauerei.Getraenk;
import lieferant.Lieferant;

public class NetterStudent extends Student implements Lieferant {

	public NetterStudent(String name, int alter) {
		super(name, alter);
	}

	/* Ein netter Student verkauft jemand anderem sein Getraenk,
	 * wenn der Name stimmt (Alter spielt keine Rolle
	 * @see lieferant.Lieferant#kaufeGetraenk(java.lang.String)
	 */
	@Override
	public Getraenk verkaufeGetraenk(String name, int alter) {
		if (getraenk == null)
			return null;

		if (getraenk.getName().equals(name)) {
			/* Jemand moechte ein Getraenk (das Alter ist hier uninteressant ;-)) */
			Getraenk g = getraenk;
			getraenk = null;
			return g;
		}

		return null;
	}

}
