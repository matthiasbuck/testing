package kunden;

public class KontoInformationen {
	int ktoNummer;
	int blz;

	public KontoInformationen(int ktoNummer, int blz) {
		if (ktoNummer < 100000 || ktoNummer > 999999) 
			throw new IllegalArgumentException(
					"Ungueltige Kontonummer oder Bankleitzahl");
		this.ktoNummer = ktoNummer;
		this.blz = blz;
	}

	public int abfragenKontonummer() {
		return (ktoNummer);
	}

	public int abfragenBlz() {
		return (blz);
	}
}
