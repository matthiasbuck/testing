package amazon.bo.usecases.reuse;

import amazon.bo.reuse.TopBookWithDetailsVeryLongList;

/**
 * Sehr lange Top Listen von Buechern
 * @author schimkar
 *
 */
public class UC212 {

	public static void main(String[] args) {
		// Im Konstruktor wird die Anzahl der Treffer in der Ergebnisliste angegeben.
		TopBookWithDetailsVeryLongList topDetails = new TopBookWithDetailsVeryLongList(60);
		topDetails.createList("object-oriented java");
		topDetails.printList2Console();
		topDetails.printList2Html();
	}
}
