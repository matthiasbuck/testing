package student;

import lieferant.Lieferant;
import anwendung.monitoring.Beobachter;
import brauerei.Getraenk;

public class Student implements Beobachter{
	protected Getraenk getraenk;
	private String name;
	private int alter;

	public Student(String name, int alter) {
		this.name = name;
		this.alter = alter;

		/* Hat bei Geburt kein Getraenk in der Hand */
		getraenk = null;
	}

	public String getName() {
		return name;
	}

	/**
	 * Student anweisen, eine Menge zu trinken
	 * @param delta
	 * @return Erfolg oder Fehler
	 */
	public boolean trinke(int delta) {
		boolean b;

		if (getraenk == null)
			return false;

		b = getraenk.reduziereMenge(delta);

		/* Wenn Getraenk leer, werfe Getraenk weg */
		if (getraenk.getMenge() == 0)
			getraenk = null;

		return b;
	}

	/**
	 * Trinke vollständige Menge des Getränks, sofern vorhanden
	 */
	public boolean trinkeAlles() {
		if (getraenk == null)
			return false;
		return trinke(getraenk.getMenge());
	}



	/**
	 * Kauft ein Getraenk beim angegebenen Lieferanten der angegebenen
	 * Sorte
	 * @param lieferant
	 * @param sorte
	 * @return Erfolg oder nicht, Getraenk wird gemerkt
	 */
	public boolean kaufeGetraenk(Lieferant lieferant, String sorte) {
		if (lieferant == null)
			return false;

		/*
		 * Wenn Student noch ein Getraenk hat, kauft er NICHT
		 * noch ein Neues. Erst austrinken!
		 */
		if (getraenk != null)
			return false;

		getraenk = lieferant.verkaufeGetraenk(sorte, getAlter());

		return (getraenk != null);
	}

	private int getAlter() {
		return(alter);
	}

	@Override
	public String status() {
		if (getraenk == null) {
			return((getName() + ": Habe kein Getraenk!"));
		}
		return(getName() + ": " + getraenk.getBeschreibung());

	}
}
