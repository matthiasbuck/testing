package test_keller;

import static org.junit.Assert.*;

import java.util.List;

import kunden.*;
import bank.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import staat.TransaktionsMelder;

public class tests {

	Bank kskBC;
	Kunde martin;
	Kunde peter;
	Konto konto;

	@Before
	public void setUp() throws Exception {
		kskBC = new Bank("Kreissparkasse Biberach", 123456);

		try {
			martin = kskBC.kontoEroeffnen("Girokonto", "Martin", 150);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPersonEroeffnetKontoOhneEinzahlung() {
		// Test ob eine Person ein Konto ohne Einzahlung er�ffnen kann
		// Erwartet: Exception lt.Klasse Konto

		try {
			peter = kskBC.kontoEroeffnen("Girokonto", "Peter", 0);
		} catch (Exception e) {
			assertEquals("Transaktion ungueltig: Konto nicht angelegt",
					e.getMessage());
		}

	}

	@Test
	public void testPersonEroeffnetKontoMitEinzahlung() throws Exception {

		// Test ob eine Person ein Konto mit Einzahlung er�ffnen kann
		peter = kskBC.kontoEroeffnen("Girokonto", "Peter", 150);
		List<KontoInformationen> kontoListe = peter.konten();
		assertEquals(1, kontoListe.size());
	}

	@Test
	public void testPersonEroeffnetMehrereKonten() {
		// Test ob eine Person mehrere Konten Einzahlung er�ffnen kann

		try {
			martin = kskBC.kontoEroeffnen("Girokonto", "Martin", 250);
			List<KontoInformationen> kontoListe = martin.konten();
			assertEquals(2, kontoListe.size());
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Test ob sich die Kontonummern unterscheiden

		// boolean unterschiedlich = false;
		// List<KontoInformationen> kontoListe = martin.konten();
		// int kto1 = kontoListe.get(0).abfragenKontonummer();
		// int kto2 = kontoListe.get(1).abfragenKontonummer();
		//
		// if(kto1 != kto2){
		// unterschiedlich = true;
		// }
		//
		// assertEquals(true, unterschiedlich);

	}

	@Test
	public void testKundeWillEinzahlungVornehmen() {

		// Kontostand vor der Einzahlung wird �berpr�ft

		int ktoNummer = martin.getStandardKonto().abfragenKontonummer();
		float KontostandAnfangs = kskBC.abfragenKontostand(ktoNummer);

		// Einzahlung wird durchgef�hrt
		kskBC.einzahlen(ktoNummer, 500);

		// Pr�fen ob sich der Kontostand ver�ndert hat

		boolean mehrWieVorher = false;
		if (KontostandAnfangs < kskBC.abfragenKontostand(ktoNummer)) {
			mehrWieVorher = true;
		}
		assertEquals(true, mehrWieVorher);

		// Anfangs 150 + 500 sollten 650 ergeben
		assertEquals(650, kskBC.abfragenKontostand(ktoNummer), 0);
	}

	@Test
	public void testTippfehlerBeiKontoNummer() {

		// Versuch ob eine Identifikation des Besitzers m�glich ist bei falscher
		// Kontonummer
		try {
			kskBC.abfragenKontoBesitzer(54321);
		} catch (IllegalArgumentException e) {
			assertEquals("Kontonummer unbekannt", e.getMessage());
		}
	}

	@Test
	public void testVersuchAbzuhebenFalscheKontonummer() {

		// Versuch Geld anhand einer falschen Kontonummer abzuheben
		// Exception erwartet, da kein Konto gefunden wird

		try {
			kskBC.abheben(54321, 100);
		} catch (Exception e) {
			assertEquals("Kontonummer unbekannt", e.getMessage());
		}
	}

	@Test
	public void testVersuchAbzuhebenKlappt() {

		// Versuch Geld vom Konto abzuheben
		// Kontostand vor der Einzahlung wird �berpr�ft

		int ktoNummer = martin.getStandardKonto().abfragenKontonummer();
		float KontostandAnfangs = kskBC.abfragenKontostand(ktoNummer);

		// Abhebung wird durchgef�hrt
		// Keine Exception zu warten
		try {
			kskBC.abheben(ktoNummer, -100);
		} catch (Exception e) {
		}

		// Pr�fen ob sich der Kontostand ver�ndert hat

		boolean wenigerWieVorher = false;
		float kontostandDanach = kskBC.abfragenKontostand(ktoNummer);
		if (KontostandAnfangs > kontostandDanach) {
			wenigerWieVorher = true;
		}
		assertEquals(true, wenigerWieVorher);

		// Anfangs 150 - 100 sollten 50 ergeben
		assertEquals(50, kskBC.abfragenKontostand(ktoNummer), 0);
	}

	@Test
	public void testVersuchAbzuheben0Euro() throws Exception {

		// Versuch Geld vom Konto abzuheben
		// Kontostand vor der Einzahlung wird �berpr�ft

		int ktoNummer = martin.getStandardKonto().abfragenKontonummer();

		// Abhebung wird durchgef�hrt
		// Keine Exception zu warten
		try {
			kskBC.abheben(ktoNummer, 0);
		} catch (IllegalArgumentException e) {
			assertEquals("Ungueltiger Betrag", e.getMessage());
		}
	}

	@Test
	public void testStringAusgabe() {

		// aktueller stand der Bank wird ausgegeben, mit Bankname, Kundenanzahl,
		// Kontenanzahl
		assertEquals("Kreissparkasse Biberach hat 1 Kunden und 1 Konten.",
				kskBC.toString());
	}

	@Test
	public void testTransfer() throws Exception {

		// Weiterer Kunde er�ffnet ein konto
		try {
			peter = kskBC.kontoEroeffnen("Girokonto", "Peter", 500);
		} catch (Exception e) {
		}

		int ktoPeter = peter.getStandardKonto().abfragenKontonummer();
		int ktoMartin = martin.getStandardKonto().abfragenKontonummer();

		// Peter �berweist Martin jetzt 300 Euro
		// Fall mit falscher Kontonummer wurde bereits abgedeckt, deswegen hier
		// nicht

		kskBC.transfer(ktoPeter, ktoMartin, 300);

		// Kontrolle der Kontost�nde

		float KontostandMartin = kskBC.abfragenKontostand(ktoMartin);
		float KontostandPeter = kskBC.abfragenKontostand(ktoPeter);

		// Bei Martin anfangs 150, mit den 300 von Peter sollten es 450 Euro
		// sein
		assertEquals(450, KontostandMartin, 0);

		// Bei Peter anfangs 500, mit den 300 an Martin sollten es noch 200 Euro
		// sein
		assertEquals(200, KontostandPeter, 0);

	}

	@Test
	public void testAnderenKontoTypAnlegen() {
		// Konto vom Typ CoolesKonto anlegen
		// Exception wird erwartet

		try {
			peter = kskBC.kontoEroeffnen("CoolesKonto", "Peter", 500);
		} catch (Exception e) {
			assertEquals("Kontotyp unbekannt", e.getMessage());
		}
	}

	@Test
	public void testSparkontoAnlegen() throws Exception {
		// Konto vom Typ Sparkonto anlegen

		peter = kskBC.kontoEroeffnen("Sparkonto", "Peter", 500);
		List<KontoInformationen> kontoListe = peter.konten();
		assertEquals(1, kontoListe.size());
	}

	@Test
	public void testSparkontoUndGiroAnlegen() throws Exception {
		// Konto vom Typ Sparkonto anlegen

		peter = kskBC.kontoEroeffnen("Sparkonto", "Peter", 500);
		peter = kskBC.kontoEroeffnen("Girokonto", "Peter", 1500);
		List<KontoInformationen> kontoListe = peter.konten();
		assertEquals(2, kontoListe.size());
	}

	@Test
	public void testSummeZuHochMeldungAnBehoerde() throws Exception{
		// Weiterer Kunde er�ffnet ein konto
		try {
			peter = kskBC.kontoEroeffnen("Girokonto", "Peter", 15000);
		} catch (Exception e) {
		}

		int ktoPeter = peter.getStandardKonto().abfragenKontonummer();
		int ktoMartin = martin.getStandardKonto().abfragenKontonummer();

		// Peter �berweist Martin jetzt 300 Euro
		// Fall mit falscher Kontonummer wurde bereits abgedeckt, deswegen hier
		// nicht

		kskBC.transfer(ktoPeter, ktoMartin, 10000);

		// Kontrolle der Kontost�nde ob der hohe Betrag durchging

		float KontostandMartin = kskBC.abfragenKontostand(ktoMartin);
		float KontostandPeter = kskBC.abfragenKontostand(ktoPeter);

		// Bei Martin anfangs 150, mit den 300 von Peter sollten es 450 Euro
		// sein
		assertEquals(10150, KontostandMartin, 0);

		// Bei Peter anfangs 500, mit den 300 an Martin sollten es noch 200 Euro
		// sein
		assertEquals(5000, KontostandPeter, 0);
		
		//Betrag ging durch
		// Kontrolle ob die Beh�rde eine Meldung gemacht hat
		
		TransaktionsMelder melder = new TransaktionsMelder();
		String test = melder.toString();
		assertEquals("Betrag ueberschreitet 5000 EURO", melder.toString());
		
		
	}
}
