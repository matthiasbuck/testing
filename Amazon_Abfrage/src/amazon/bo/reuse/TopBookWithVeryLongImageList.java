package amazon.bo.reuse;

import amazon.bo.AmazonProduct;
import amazon.bo.AmazonResult;
import amazon.export.ImageLayout;

public class TopBookWithVeryLongImageList extends
		TopBookWithDetailsVeryLongList {

	public TopBookWithVeryLongImageList(int max) {
		super(max);
	}


	@Override
	public void createList(String keywords) {
		search.setKeywords(keywords);

		int pages = max / 10;
		int rest = max % 10;

		AmazonProduct[] myProducts = new AmazonProduct[max];

		for (int j = 0; j < pages; j++) {
			AmazonResult res = service.request(search, (j + 1));
			AmazonProduct[] p = res.getProducts();
			for (int i = 0; i < p.length; i++) {
				myProducts[i + (10 * j)] = p[i];
			}

		}
		AmazonResult res = service.request(search, (pages+1));
		AmazonProduct[] p = res.getProducts();
		for (int i = 0; i < rest; i++) {
			myProducts[i + (10 * pages)] = p[i];
		}

		this.result = new AmazonResult();
		this.result.setBooks(myProducts);
	}

	public void printList2Html() {
		ImageLayout out = new ImageLayout(search.getKeywords());
		out.generateHtmlLayout(result.getProducts());
		out.createHtmlFile(search.getKeywords() + ".html");

	}

}
