package staat;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 
 * Implementierung der Klasse darf nicht verändert werden.
 */
public class TransaktionsMelder {
	// Liste von Meldungen an die Behörde
	private List<String> liste = new LinkedList<String>();
	final static List<String> testing = new LinkedList<String>();
	/**
	 * Nachricht über verdächtige Transaktionen an die Finanzaufsicht, die nicht
	 * besonders effizient arbeitet
	 * 
	 * @param nachricht
	 *            Nachricht an die Finanzaufsicht
	 */
	public void melden(String nachricht) {
		try {
			for (int i = 1; i <= 2; i++) {
				TimeUnit.SECONDS.sleep(1);
				System.out.println(i + " sec ... ");
			}
		} catch (InterruptedException e) {
			;
		} finally {
			liste.add(nachricht);
			testing.add(nachricht);
		}
	}

	public String toString() {
		StringBuffer buf = new StringBuffer();
		for (String s : testing)
			buf.append(s);
		return (buf.toString());
	}

}
