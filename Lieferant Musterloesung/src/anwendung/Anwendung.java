package anwendung;

import laden.Supermarkt;
import student.NetterStudent;
import student.Student;
import brauerei.Brauerei;

public class Anwendung {
	public static void main(String args[]) {
		Brauerei brauereiMeckatzer = new Brauerei("Meckatzer");
		NetterStudent peter = new NetterStudent("Peter", 20);
		Student klaus = new Student("Klaus", 17);
		Supermarkt kaufland = new Supermarkt("Kaufland",
				brauereiMeckatzer);

		Szenarien sz = new Szenarien();
	//	sz.einfachesKaeuferSzenario(kaufland, peter, klaus, peter);

		sz.studentKauftGetraenk(peter, kaufland, "Bier");
		sz.studentKauftGetraenk(peter, kaufland, "Cola");
		sz.studentKauftGetraenk(peter, kaufland, "Kamillentee");

		sz.studentKauftGetraenk(klaus, kaufland, "Bier");
		sz.studentKauftGetraenk(klaus, kaufland, "Cola");
		sz.studentKauftGetraenk(klaus, kaufland, "Kamillentee");


		sz.studentKauftGetraenk(peter, brauereiMeckatzer, "Bier");
		sz.studentKauftGetraenk(peter, brauereiMeckatzer, "Cola");
		sz.studentKauftGetraenk(peter, brauereiMeckatzer, "Kamillentee");

		sz.studentKauftGetraenk(klaus, brauereiMeckatzer, "Bier");
		sz.studentKauftGetraenk(klaus, brauereiMeckatzer, "Cola");
		sz.studentKauftGetraenk(klaus, brauereiMeckatzer, "Kamillentee");

		sz.studentKauftGetraenk(klaus, peter, "Bier");

		peter.kaufeGetraenk(kaufland, "Bier");
		peter.trinke(100);
		sz.studentKauftGetraenk(klaus, peter, "Bier");

		sz.studentKauftGetraenk(klaus, peter, "Cola");
		sz.studentKauftGetraenk(klaus, peter, "Kamillentee");


	}
}
