package laden;

import anwendung.monitoring.Beobachter;
import brauerei.Brauerei;
import brauerei.Getraenk;
import lieferant.Lieferant;

public class Supermarkt implements Lieferant, Beobachter {
	private static final int SORTIMENT_SIZE = 1;
	private Brauerei brauerei;
	private String name;
	private Getraenk sortiment[];

	public Getraenk[] getSortiment() {
		return sortiment;
	}

	public void setSortiment(Getraenk[] sortiment) {
		this.sortiment = sortiment;
	}

	public Supermarkt(String name, Brauerei brauerei) {
		if (name == null){
			throw new IllegalArgumentException("Kein Name eingegeben!");
		}
		
		this.name = name;
		this.brauerei = brauerei;
		/* Erstelle Platz fuer SORTIMENT_SIZE Getraenke */
		sortiment = new Getraenk[SORTIMENT_SIZE];

		fuelleSortiment();
	}

	/**
	 * Fuellt das sortiment[]-Array mit 50% Cola und 50% Bier auf,
	 * welches von der Brauerei geholt wird
	 */
	public void fuelleSortiment() {
		for (int i=0; i < SORTIMENT_SIZE; i++) {
			if (i%2 == 0)
				sortiment[i] = brauerei.verkaufeGetraenk(Getraenk.BIER, 16);
			else
				sortiment[i] = brauerei.verkaufeGetraenk(Getraenk.COLA, 1);
		}
	}


	/**
	 * Sucht ein Getraenk mit "name" im Sortiment und gibt es zurueck
	 */
	@Override
	public Getraenk verkaufeGetraenk(String name, int alter) {
		for (int i=0; i < SORTIMENT_SIZE; i++) {
			if (sortiment[i] != null) {
				if (sortiment[i].getName().equals(name) && alter >= 18) {
					/*
					 * Entferne gefundenes Getraenk aus Sortiment und
					 * gebe es an Aufrufer zurueck
					 */
					Getraenk g = sortiment[i];
					sortiment[i] = null;
					return g;
				}
			}
		}
		return null;
	}

	@Override
	public String status() {
		StringBuffer s = new StringBuffer("----- Sortiment Supermarkt " + getName() + "-----");

		if (brauerei != null)
			s.append("\n Liefernde Brauerei: " + brauerei.getName());

		for (int i=0; i < SORTIMENT_SIZE; i++) {
			if (sortiment[i] != null)
				s.append("\n[#" + i + "] " + sortiment[i].getBeschreibung());
		}

		s.append("\n");
		return(s.toString());
	}

	public String getName() {
		return(name);
	}
}
