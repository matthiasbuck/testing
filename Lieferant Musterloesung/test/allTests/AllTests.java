package allTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({anwendung.AnwendungTest.class, brauerei.TestBrauerei.class, laden.TestSupermarkt.class, student.NetterStudentTest.class, student.StudentTest.class})
public class AllTests {

}
