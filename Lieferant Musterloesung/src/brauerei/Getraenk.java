package brauerei;

public abstract class Getraenk {
	/**
	 * Konstanten fuer einzelne Getraenke
	 */
	public static final String COLA = "Cola";
	public static final String BIER = "Bier";

	private int menge;

	/* Konstruktor ist protected, d.h. nur innerhalb des Packages erreichbar */
	protected Getraenk(int menge) {
		this.menge = menge;
	}

	/**
	 * @return gibt die aktuelle Menge zurück
	 */
	public int getMenge() {
		return menge;
	}

	/**
	 * @param delta reduziert die Menge um delta, sofern möglich
	 * @return Erfolgstatus
	 */
	public boolean reduziereMenge(int delta) {
		if (delta < 0 || delta > menge)
			return false;
		menge = menge - delta;
		return true;
	}

	/**
	 * @return Gibt die Bezeichnung des Getränks zurück (z.B. "Bier").
	 *         Wird in Kindklasse implementiert, deshalb abstrakt
	 */
	public abstract String getName();

	/**
	 * @return Jedes Getränk kann sich selber beschreiben, z.B.:
	 *         "Bin ein Bier mit 500 ml und 10% Alkohol"
	 *         Kann nur in Kindklasse implementiert werden, deshalb abstrakt
	 */
	public abstract String getBeschreibung();
}
