package amazon.bo.usecases.reuse;


import amazon.bo.reuse.TopBookWithDetails;

public class UC205 {
	public static void main(String[] args) {
		TopBookWithDetails top = new TopBookWithDetails();
		// Liste fuer Lenny Kravitz erstellen
		top.createList("apple");
		// Liste auf der Console ausgeben
		top.printList2Console();
		// Liste in einer Web-Seite ausgeben
		top.printList2Html();
	}
}
