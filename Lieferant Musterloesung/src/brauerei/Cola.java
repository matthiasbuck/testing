package brauerei;

public class Cola extends Getraenk {
	private int koffein;

	public Cola(int menge, int koffein) {
		super(menge);
		this.koffein = koffein;
	}

	/* Standard Cola hat 10 Einheiten Koffein */
	public Cola(int menge) {
		this(menge, 10);
	}

	public int getKoffein() {
		return koffein;
	}

	@Override
	/* Wir muessen die geerbte abstrakte Methode getName() implementieren */
	public String getName() {
		return Getraenk.COLA;
	}

	/* Wir muessen die geerbte abstrakte Methode getBeschreibung() implementieren */
	@Override
	public String getBeschreibung() {
		/* "500 ml Cola, mit 10mg/100g Koffein" */
		return getMenge() + " ml " + getName() + ", mit " + getKoffein() + "mg/100g Koffein";
	}

}
