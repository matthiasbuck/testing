package brauerei;

import lieferant.Lieferant;

public class Brauerei implements Lieferant {
	private String name;

	public Brauerei(String name) {
		setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Brauerei kann Getraenke erstellen; Sie lagert aber keine Getraenke
	 */
	@Override
	public Getraenk verkaufeGetraenk(String name, int alter) {

		if (name.equals(Getraenk.COLA))
			return GetraenkeFactory.erzeugeGetraenk(Getraenk.COLA, 1000);

		if (name.equals(Getraenk.BIER) && alter >= 18)
			return GetraenkeFactory.erzeugeGetraenk(Getraenk.BIER, 500);
		return new LeeresGetraenk();
	}


}
