import kunden.Kunde;
import bank.Bank;

public class AnwendungSzenario {

	public static void main(String[] args) throws Exception {

		Bank sparkasseWgt = new Bank("Sparkasse Weingarten", 12345);
		
		// Mike er�ffnet ein Konto mit einer initialen Einzahlung von 100 
		Kunde mike = sparkasseWgt.kontoEroeffnen("GiroKonto", "Mike", 100);

		// Mike f�hrt weitere Einzahlungen durch
		int mikesKtoNummer = mike.getStandardKonto().abfragenKontonummer();
		sparkasseWgt.einzahlen(mikesKtoNummer, 1000);
		
		// Mike hebt 200,50 Euro von seinem Konto ab => Betrag wird als negative Zahl angegeben
		sparkasseWgt.abheben(mikesKtoNummer, -200.5f);  // Betrag wird als float angegeben
		try {
			// Mike vertippt sich bei der Eingabe der Kontonummer
			sparkasseWgt.abheben(123123, (float) 200.5);
		} catch (Exception e) {
			// Er hat nochmal einen Versuch, die richtige Kontonummer anzugeben
			sparkasseWgt.abheben(mikesKtoNummer, -200.5f);
		}

		// Wie hoch ist der aktuelle Kontostand => 699,00  Euro
		System.out.println(sparkasseWgt.abfragenKontostand(mikesKtoNummer));

		// Verd�chtige Transaktion m�sste eigentlich an die Beh�rde gemeldet werden
		sparkasseWgt.einzahlen(mikesKtoNummer, 6000);
		
	}

}
