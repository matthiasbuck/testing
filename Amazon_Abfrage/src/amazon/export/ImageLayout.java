package amazon.export;

import amazon.bo.AmazonProduct;

public class ImageLayout extends HtmlOutput {

	public ImageLayout(String title) {
		super(title);
	}

	@Override
	public boolean generateHtmlLayout(AmazonProduct[] prod) {
		if (prod == null || prod.length == 0)
			return (false);
		writer.write("<div>");
		for (int i = 0; i < prod.length; i++) {
			StringBuffer buf = new StringBuffer(100);
			AmazonProduct p = prod[i];
			if (p != null) {
				if (p.getImage() != null && p.getUrl() != null)
					buf.append("<a href=\"" + p.getUrl() + "\">  <img src=\""
							+ p.getImage() + "\"  title=\"" + p.getTitle()
							+ "\"> </a>");
			}
			appendItem(buf.toString());
		}
		writer.write("</div>");
		return (true);
	}

	public void appendItem(String item) {
		if (writer != null)
			writer.write(item + "\n");
	}

}
