package student;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import brauerei.Cola;
import brauerei.Getraenk;

public class NetterStudentTest {

	NetterStudent student;
	
	@Before
	public void setUp() throws Exception {
	student = new NetterStudent("Hans", 18);
	}	

	@Test
	public void testNetterStudent() {
		assertTrue(student.getClass().getSuperclass().equals(Student.class));
		assertEquals(NetterStudent.class, student.getClass());
		assertNotEquals(Student.class, student.getClass());
		assertNotNull(student);
		
	}

	@Test
	public void testVerkaufeGetraenk() {
		
		student.getraenk = new Cola(5);
		
		assertEquals(student.getraenk.getClass(), Cola.class);
		assertNotNull(student.getraenk);
		assertNotNull(student.verkaufeGetraenk("Cola", 18));
		assertNull(student.verkaufeGetraenk(null, 5));
		assertNull(student.verkaufeGetraenk("Cola", 0));
		assertNull(student.verkaufeGetraenk("Bier", 18));
		assertNull(student.getraenk);
		
	}

}
