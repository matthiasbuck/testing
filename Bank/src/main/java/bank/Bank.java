package bank;

import java.util.ArrayList;
import java.util.List;

import staat.TransaktionsMelder;
import kunden.Kunde;

/**
 * 
 * Klasse Bank
 */
public class Bank {
	private List<Kunde> kunden; // Kundenliste
	private List<Konto> konten; // Liste von Konten
	private String name;// Name der Bank
	private int blz; // BLZ der Bank

	// staatliches Finanzaufsichtsbeh�rde
	private TransaktionsMelder melder = new TransaktionsMelder();

	// Zaehler f�r die Vergabe von Kontonummern
	private int zaehler = 100000;

	/**
	 * 
	 * @param name Name der Bank
	 * @param blz BLZ der Bank
	 */
	public Bank(String name, int blz) {
		this.name = name;
		this.blz = blz;
		kunden = new ArrayList<Kunde>();
		konten = new ArrayList<Konto>();
	}

	public void setzeTransaktionsmelder(TransaktionsMelder melder) {
		this.melder = melder;
	}

	/**
	 * 
	 * @param kontoArt SparKonto oder GiroKonto
	 * @param kundenName Name des Kunden
	 * @param betrag Höhe der initialen Einzahlung
	 * @return Bestehender Kunde oder Neukunde
	 * @throws Exception
	 */
	public Kunde kontoEroeffnen(String kontoArt, String kundenName, float betrag)
			throws Exception {
		Konto konto = KontoFactory.ermittleKontoArt(kontoArt, betrag);

		// Check Kunde
		Kunde kunde = ermittleKunde(kundenName);

		// Kontonummer
		int ktoNummer = zaehler++;
		konto.setzeNummer(ktoNummer);
		konto.setzeKunde(kunde);
		konten.add(konto);

		kunde.add(ktoNummer, blz);
		kunden.add(kunde);
		return kunde;
	}

	/**
	 * 
	 * @param ktoNummer Kontonummer
	 * @return aktueller Kontostand
	 */
	public float abfragenKontostand(int ktoNummer) {
		Konto konto = ermittleKonto(ktoNummer);
		if (konto == null)
			throw new IllegalArgumentException("Kontonummer unbekannt");
		return konto.abfragenKontostand();
	}

	public String abfragenKontoBesitzer(int ktoNummer) {
		for (Konto k : konten)
			if (k.abfragenKontonummer() == ktoNummer)
				return (k.abfragenBesitzer().getName());
		throw new IllegalArgumentException("Kontonummer unbekannt");
	}

	/**
	 * 
	 * @param ktoNummer
	 * @param betrag Betrag, der abgehoben wird. Betrag sollte als negative Zahl angegeben werden.
	 * @throws Exception
	 */
	public void abheben(int ktoNummer, float betrag) throws Exception {
		Konto konto = ermittleKonto(ktoNummer);
		if (konto == null)
			throw new Exception("Kontonummer unbekannt");
		konto.abheben(betrag);
	}

	/**
	 * 
	 * @param ktoNummer
	 * @param betrag Höhe des Betrags für die Einzahlung
	 */
	public void einzahlen(int ktoNummer, float betrag) {
		Konto konto = ermittleKonto(ktoNummer);
		konto.einzahlen(betrag);
	}

	/**
	 * 
	 * @param qKtoNummer Quelle-Kontonummer
	 * @param zKtoNummer Kontonummer des Zielkontos
	 * @param betrag Höhe der Überweisung
	 * @throws Exception Ausnahme, wenn Kontonummern ungültig oder Konten nicht gedeckt sind.
	 */
	public void transfer(int qKtoNummer, int zKtoNummer, float betrag)
			throws Exception {
		Konto qKonto = ermittleKonto(qKtoNummer);
		Konto zKonto = ermittleKonto(zKtoNummer);

		qKonto.abheben(betrag * -1);
		zKonto.einzahlen(betrag);
	}

	

	/**
	 * 
	 * @param kundenName
	 *            Name des Kunden
	 * @return bestehender Kunde oder Neukunde
	 */
	private Kunde ermittleKunde(String kundenName) {
		for (Kunde k : kunden) {
			if (k.getName().equals(kundenName))
				return (k);
		}
		Kunde neuerKunde = new Kunde(kundenName);
		return neuerKunde;
	}

	/**
	 * 
	 * @param ktoNummer
	 * @return Passendes Konto zu gegebener Kontonummer falls kein Konto zu
	 *         gegebener Kontonummer existiert, wird null zurückgegeben
	 */
	private Konto ermittleKonto(int ktoNummer) {
		for (Konto k : konten)
			if (k.abfragenKontonummer() == ktoNummer)
				return (k);
		return null;
	}

	@Override
	public String toString() {
		return(name + " hat " + kunden.size() + " Kunden und " + konten.size() + " Konten.");
	}

	public Object getTransaktionsMelder() {
		return this.melder;
	}
}
