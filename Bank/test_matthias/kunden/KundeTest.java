package kunden;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class KundeTest {

	Kunde kunde;
	List <KontoInformationen> kontoinformationen; 
	
	@Before
	public void setUp() throws Exception {
		kunde = new Kunde("Hans");
	}

	@Test
	public void testAdd() {
		kunde.add(123451, 1234511);
		
		assertNotNull(kunde.konten());
		assertTrue(kunde.konten().get(0).blz == new KontoInformationen(123451, 1234511).blz);
		
	}
	
	@Test
	public void testAddFalscheKNR() {
		try {
			kunde.add(1, 12345);
			fail("sollte nicht klappen");
		} catch (IllegalArgumentException e) {
			
			assertEquals(e.getMessage(), "Ungueltige Kontonummer oder Bankleitzahl");
		}
		
		try {
			kunde.add(1111111, 1);
			fail("sollte nicht klappen");
		} catch (IllegalArgumentException e) {
			
			assertEquals(e.getMessage(), "Ungueltige Kontonummer oder Bankleitzahl");
		}		
	}

	@Test
	public void testKonten() {
		kunde.add(123451, 1234511);
		kontoinformationen = kunde.konten();
		
		assertEquals(kontoinformationen.get(0).blz, 1234511);
		assertEquals(kontoinformationen.get(0).ktoNummer, 123451);
	}

	@Test
	public void testGetStandardKonto() {
		
	}

	@Test
	public void testGetName() {
		
	}

}
