package bank;

import java.util.LinkedList;
import java.util.List;

import staat.TransaktionsMelder;
import kunden.Kunde;

public abstract class Konto {
	private int nummer; // Kontonummer
	private Kunde kunde; // Besitzer des Konto
	/**
	 * Liste von Transaktionen (Ein- und Auszahlungen), die auf dem Konto durchgeführt wurden
	 */
	protected  List<Transaktion> transaktionen = new LinkedList<Transaktion>();
	
	/**
	 * 
	 * @param t Transaktion bei der Eröffnung des Konto
	 * @throws Exception
	 */
	protected Konto(Transaktion t) throws Exception {
		if (t.betrag > 0)
				transaktionen.add(t);
		else
			throw new Exception("Transaktion ungueltig: Konto nicht angelegt");
	}
	
	protected void setzeKunde(Kunde k) {
		this.kunde = k;
	}

	void einzahlen(float betrag) {
		if (betrag<=0) {
			throw new IllegalArgumentException("Ungueltige Einzahlung");
		} else if(betrag > 5000){
			TransaktionsMelder melder = new TransaktionsMelder();
			melder.melden("Betrag ueberschreitet 5000 EURO");
		}
		transaktionen.add(new Transaktion(betrag));
	}
	
	/**
	 * Kontostand wird auf Basis der Transaktionen stets neu berechnet
	 * @return
	 */
	float abfragenKontostand() {
		float stand = 0;
		for (Transaktion t: transaktionen) {
			stand += t.betrag;
		}
		return stand;
	}
	
	public void setzeNummer(int ktoNummer) {
		this.nummer = ktoNummer;
	}
	
	public int abfragenKontonummer() {
		return(nummer);
	}


	public Kunde abfragenBesitzer() {
		return kunde;
	}
	
	/**
	 * 
	 * @param betrag Betrag, der vom Konto abgehoben wird.
	 * @throws Exception
	 */
	abstract void abheben(float betrag) throws Exception;


}
