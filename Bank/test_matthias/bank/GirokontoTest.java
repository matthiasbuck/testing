package bank;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

public class GirokontoTest {
	
	GiroKonto girokonto;
	Transaktion t;
	
	@Before
	public void setUp() throws Exception {
		t = mock(Transaktion.class);
		t.betrag=100;
		t.datum=new Date();
	}

	@Test
	public void testAbheben() {
		
		try {
			girokonto = new GiroKonto(t);
		} catch (Exception e1) {
			fail("should workkkk");
		}
		
		try {
			girokonto.abheben(-100);
		} catch (Exception e) {
			fail("should work");
		}
		
		try {
			girokonto.abheben(100000000);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Ungueltiger Betrag");
		}
		
	}

}
