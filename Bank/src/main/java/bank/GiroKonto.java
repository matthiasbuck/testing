package bank;


public class GiroKonto extends Konto {
	/**
	 * Standard-Dispo
	 */
	public static final float STANDARD_DISPO = -500;
	/**
	 * Maximal möglicher Dispo
	 */
	public static final float MAX_DISPO = -5000;
	private float dispo = STANDARD_DISPO; // Jedes Girokonto besitzt  einen Dispo


	protected GiroKonto(Transaktion t) throws Exception {
		this(t, STANDARD_DISPO);
	}
	
	protected GiroKonto( Transaktion t, float dispo) throws Exception {
		super(t);
		if (dispo < MAX_DISPO)
			throw new IllegalArgumentException("Ungueltiger Dispo");
		this.dispo = dispo;
	}


	@Override
	void abheben(float betrag) throws Exception {
		if (betrag >= 0)
			throw new IllegalArgumentException("Ungueltiger Betrag");
		float stand = abfragenKontostand();
		if (stand + betrag < dispo )
			throw new Exception("Dispo ueberschritten");
		transaktionen.add(new Transaktion(betrag));
	}
}
