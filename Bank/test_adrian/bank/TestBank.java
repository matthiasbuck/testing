package bank;

import static org.junit.Assert.*;
import kunden.Kunde;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import staat.TransaktionsMelder;

public class TestBank {

	@Mock
	private TransaktionsMelder transaktionsMelder;
	
	@InjectMocks
	private Bank bank;
	
	@Before
	public void setUp() throws Exception {
		bank = new Bank("Bank", 8000);
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testBankErstellen() {
		assertEquals(Bank.class, bank.getClass());
		assertNotNull(bank);
	}

	@Test
	public void testSetzeTransaktionsmelder() {
		TransaktionsMelder melder = new TransaktionsMelder();
		bank.setzeTransaktionsmelder(melder);
		assertEquals(melder, bank.getTransaktionsMelder());
	}

	@Test
	public void testKontoEroeffnenMitPositivenGuthaben(){
		try {
			Kunde mike = bank.kontoEroeffnen("Girokonto", "Mike", 100);
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	
	@Test
	public void testKontoEroeffnenMit0Euro() {
		try {
			assertEquals(new IllegalArgumentException("Ungueltige Einzahlung"), bank.kontoEroeffnen("Girokonto", "Mark", 0));
		} catch (Exception e) {
			//e.printStackTrace();
		}

	}
	
	@Test
	public void testKontoEroeffnenMitNegativenGuthaben() {
		try {
			assertEquals(new IllegalArgumentException("Ungueltige Einzahlung"), bank.kontoEroeffnen("Girokonto", "Merk", -100));
		} catch (Exception e) {
			//e.printStackTrace();
		}

	}
	
	@Test
	public void testAbfragenKontostand() {
		
		try {
			Kunde mike = bank.kontoEroeffnen("Girokonto", "Mike", 100);
		} catch (Exception e) {
			//e.printStackTrace();
		}
		//bank.abfragenKontostand(ktoNummer) hier bin ich grad
	}

	@Test
	public void testAbfragenKontoBesitzer() {
		fail("Not yet implemented");
	}

	@Test
	public void testAbheben() {
		fail("Not yet implemented");
	}

	@Test
	public void testEinzahlen() {
		fail("Not yet implemented");
	}

	@Test
	public void testTransfer() {
		fail("Not yet implemented");
	}

	@Test
	public void testToString() {
		fail("Not yet implemented");
	}

}
