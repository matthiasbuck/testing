package bank;


public class SparKonto extends Konto {
	/**
	 * Standard-Dispo
	 */
	public static final float DISPO = 0;
	private float dispo = DISPO; // Ein Sparkonto besitzt  keinen Dispo


	protected SparKonto(Transaktion t) throws Exception{
		super(t);
	}


	@Override
	void abheben(float betrag) throws Exception {
		if (betrag >= 0)
			throw new IllegalArgumentException("Ungueltiger Betrag");
		float stand = abfragenKontostand();
		if (stand + betrag < dispo )
			throw new Exception("Das Konto darf nicht �berzogen werden");
		transaktionen.add(new Transaktion(betrag));
	}
}
