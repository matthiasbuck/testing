package brauerei;

/**
 * Die Factory erzeugt anhand eines Strings die einzelnen Getraenkesorten.
 * Der Besteller eines Getraenke schickt eine Nachricht (String) und erhaelt
 * ein Getraenk (Rueckgabetyp ist Getraenk)
 *
 */
public class GetraenkeFactory {

	public static Getraenk erzeugeGetraenk(String name, int menge) {
		if (name.equals(Getraenk.COLA))
			return new Cola(menge);

		if (name.equals(Getraenk.BIER))
			return new Bier(menge);
		return(new LeeresGetraenk());
	}

}
