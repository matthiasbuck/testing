package brauerei;

public class Bier extends Getraenk {
	private int alkohol;

	/*
	 * Der Konstruktor ist protected:
	 * Nur Klassen aus demselben Package koennen Instanzen erstellen,
	 * hier in diesem Beispiel eine Brauerei
	 */
	public Bier(int menge, int alkohol) {
		super(menge);
		this.alkohol = alkohol;
	}

	/* Standard Bier hat 5 % Alkohol */
	public Bier(int menge) {
		this(menge, 5);
	}

	public int getAlkohol() {
		return alkohol;
	}

	/* Wir müssen die geerbte abstrakte Methode getName() implementieren */
	@Override
	public String getName() {
		return Getraenk.BIER;
	}

	/* Wir müssen die geerbte abstrakte Methode getBeschreibung() implementieren */
	@Override
	public String getBeschreibung() {
		/* "500 ml Bier, mit 7% Alkohol" */
		return getMenge() + " ml " + getName() + ", mit " + getAlkohol() + "% Alkohol";
	}
}
